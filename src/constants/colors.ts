export interface Colors {
  red: string;
  blue: string;
  green: string;
}

export const colorsRGB: Colors = {
  red: "#f00",
  blue: "#2900ff",
  green: "#1aff00"
};

export const colorsWords: Colors = {
  red: "Red",
  blue: "Blue",
  green: "Green"
};
