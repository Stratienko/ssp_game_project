import React from "react";
import { Colors, colorsRGB, colorsWords } from "./../constants/colors";

interface State {
  hasGameStarted: boolean;
  hasGameEnded: boolean;
  isStepCompleted: boolean;
  score: number;
  lifes: number;
  stepInfo?: StepInfo;
}

interface StepInfo {
  word: keyof Colors;
  paint: {
    paintValue: keyof Colors;
    paintColor: string;
  };
}

class App extends React.Component<any, State> {
  state: State = {
    hasGameStarted: false,
    hasGameEnded: false,
    isStepCompleted: false,
    score: 0,
    lifes: 3
  };

  private Timer: NodeJS.Timeout;

  render() {
    const { hasGameStarted, hasGameEnded, stepInfo, score, lifes } = this.state;
    return (
      <div className="App">
        {hasGameEnded ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <div className={"endGame"}>YOU DIED</div>
            <div className={"endGame"}>Your score: {score}</div>
            <button className={"startBtn"} onClick={this.restart}>
              Start again
            </button>
          </div>
        ) : (
          <div className={"containerMain"}>
            {hasGameStarted && stepInfo ? (
              <div
                onClick={this.handleLeftClick}
                onContextMenu={this.handleRightClick}
              >
                <div
                  className={"mainTitle"}
                  style={{ color: stepInfo.paint.paintColor }}
                >
                  {stepInfo.word.toUpperCase()}
                </div>
                <div className={"infoContainer"}>
                  <div className={"infoBlock"}>Your score: {score}</div>
                  <div className={"infoBlock"}>Remaining lifes: {lifes}</div>
                </div>
              </div>
            ) : (
              <button className={"startBtn"} onClick={this.start}>
                Start
              </button>
            )}
          </div>
        )}
      </div>
    );
  }

  private getStepInfo = (): StepInfo => {
    const Paints = Object.keys(colorsRGB);
    const Words = Object.keys(colorsWords);

    const getRandomValue = (sourceArray: string[]): number =>
      Math.floor(Math.random() * Math.floor(sourceArray.length));

    const paintValue: keyof Colors = Paints[
      getRandomValue(Paints)
    ] as keyof Colors;

    return {
      word: Words[getRandomValue(Words)] as keyof Colors,
      paint: {
        paintValue,
        paintColor: colorsRGB[paintValue]
      }
    };
  };

  private start = (): void => {
    this.setState({ hasGameStarted: true });

    this.updateStep();
  };

  private updateStep = (): void => {
    clearTimeout(this.Timer);

    const stepInfo = this.getStepInfo();
    this.setState({ stepInfo });

    this.Timer = setTimeout(() => {
      this.decreaseLife();
      this.updateStep();
    }, 1500);
  };

  private handleLeftClick = () => {
    const { stepInfo } = this.state;
    this.setState({ isStepCompleted: true });

    if (stepInfo && stepInfo.paint.paintValue === stepInfo.word) {
      this.increaseScore();
    } else {
      this.decreaseLife();
    }

    this.updateStep();
  };

  private handleRightClick = (e: React.SyntheticEvent) => {
    const { stepInfo } = this.state;
    e.preventDefault();
    this.setState({ isStepCompleted: true });

    if (stepInfo && stepInfo.paint.paintValue !== stepInfo.word) {
      this.increaseScore();
    } else {
      this.decreaseLife();
    }

    this.updateStep();
  };

  private restart = () => window.location.reload();

  private increaseScore = (): void =>
    this.setState({ score: this.state.score + 1 });

  private decreaseLife = (): void => {
    if (this.state.lifes > 1) {
      this.setState({ lifes: this.state.lifes - 1 });
      return;
    }

    clearTimeout(this.Timer);
    this.setState({ hasGameEnded: true });
  };
}

export default App;
